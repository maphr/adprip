# adprip.fr archives readme

Le script https://www.adprip.fr/update télécharge, anonymise et compile les
données des votants dans le dossier https://www.adprip.fr/archives/
Le script est lancé chaque jour par cron.


Le nom du fichier est `UNIX_TIMESTAMP.dat.gz` ou `UNIX_TIMESTAMP` est le nombre
de secondes écoulées depuis le 1er janvier 1970 lorsque le fichier a été
enregistré. Les fichiers sont compressés au format gzip. Chaque fichier
contient sur chaque ligne un code INSEE, suivie du nom de la localité,
suivie du nombre d'inscrit sur liste électorale (données INSEE; sauf pour
les arrondissements de Paris/Lyon/Marseille où j'ai pris les chiffres du
premier tour de l'élection de 2017), suivie du nombre de soutien.
Les champs sont séparés par des point-virgules.

(NB: les deux premiers fichiers sont le vieux format code_insee:nombre_soutien)

## Note concernant RGPD/CNIL/etc:

L'article qui semble relevant est le suivant :

> Art. L. 558-42 du code électoral - Le fait, dans le cadre des mêmes
> opérations, de reproduire des données collectées à d'autres fins que
> celles de vérification et de contrôle ou de tenter de commettre cette
> reproduction est puni de cinq ans d'emprisonnement et de 75 000 €
> d'amende.

Le but ici est celui de vérification et de contrôle - c'est pour cela que les
données ne comprennent que les localités (i.e. pour vérifier l'absence
d'anomalie statistique, par exemple qu'une certaine localité soit empêchée de
voter).
C'est la raison principale pour laquelle le script faisant tourner le site est
open-source - pour ne pas être accusé de collecter des données à tort et à
travers.

De plus nous n'enregistrons aucune donnée nominative, aucune donnée concernant
les visiteurs du site adprip.fr (cookies ou autre - hormis les logs OVH
(apache?) qui sont hors de notre ressort), ce qui semble éliminer les problèmes
de RGPD et de CNIL. Si vous êtes un avocat/une personne bien placée pour en savoir
plus n'hésitez pas à me contacter (via gitlab).

